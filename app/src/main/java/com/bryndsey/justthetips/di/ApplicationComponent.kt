package com.bryndsey.justthetips.di

import com.bryndsey.justthetips.input.InputSwitcherView
import com.bryndsey.justthetips.tipdisplay.TipDisplayView
import com.bryndsey.justthetips.input.tipinput.TipInputView
import com.bryndsey.justthetips.input.totalinput.TotalInputFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun inject(inputSwitcherView: InputSwitcherView)

    fun inject(tipDisplayView: TipDisplayView)

    fun inject(tipInputView: TipInputView)

    fun inject(totalInputFragment: TotalInputFragment)
}