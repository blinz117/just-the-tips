package com.bryndsey.justthetips.tipdisplay

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bryndsey.justthetips.JustTheTipsApplication
import com.bryndsey.justthetips.R
import easymvp.annotation.FragmentView
import easymvp.annotation.Presenter
import kotlinx.android.synthetic.main.tip_display.*
import javax.inject.Inject

@FragmentView(presenter = TipDisplayPresenter::class)
class TipDisplayView : Fragment(), TipDisplayViewInterface {

    @Inject
    @Presenter
    lateinit var presenter: TipDisplayPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        JustTheTipsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.tip_display, container)
    }

    override fun updateAmounts(viewModel: TipViewModel) {
        pretip_total_display.text = viewModel.pretipTotal
        tip_amount_display.text = viewModel.tipAmount
        total_display.text = viewModel.total
    }
}