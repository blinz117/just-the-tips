package com.bryndsey.justthetips.tipdisplay

interface TipDisplayViewInterface {

    fun updateAmounts(viewModel: TipViewModel)
}