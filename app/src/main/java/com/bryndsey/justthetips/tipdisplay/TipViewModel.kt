package com.bryndsey.justthetips.tipdisplay

data class TipViewModel(val pretipTotal: String, val tipAmount: String, val total: String)