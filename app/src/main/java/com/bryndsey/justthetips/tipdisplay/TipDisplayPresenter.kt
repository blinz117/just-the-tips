package com.bryndsey.justthetips.tipdisplay

import com.bryndsey.justthetips.data.Tip
import com.bryndsey.justthetips.data.TipCalculator
import easymvp.RxPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class TipDisplayPresenter @Inject constructor(tipCalculator: TipCalculator)
    : RxPresenter<TipDisplayViewInterface>() {

    val CURRENCY_SYMBOL = "$"

    private var tip: Tip? = null

    init {
        val tipCalculationSubscription = tipCalculator.getTipAmountUpdates()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newTip ->
                    tip = newTip
                    updateTip(newTip)
                }

        addSubscription(tipCalculationSubscription)
    }

    override fun onViewAttached(view: TipDisplayViewInterface?) {
        super.onViewAttached(view)

        if (tip != null) {
            updateTip(tip!!)
        }
    }

    private fun updateTip(tip: Tip) {
        val tipViewModel = TipViewModel(
                CURRENCY_SYMBOL + tip.pretipTotal.toString(),
                CURRENCY_SYMBOL + tip.tipAmount.toString(),
                CURRENCY_SYMBOL + tip.total.toString())
        view?.updateAmounts(tipViewModel)
    }
}