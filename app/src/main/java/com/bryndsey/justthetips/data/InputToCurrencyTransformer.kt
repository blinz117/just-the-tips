package com.bryndsey.justthetips.data

import javax.inject.Inject

class InputToCurrencyTransformer @Inject constructor() {

    private val MINIMUM_DECIMAL_PLACES = 3

    fun getCurrencyStringFromInput(inputBuffer: List<Int>): String {
        var currencyString = inputBuffer.joinToString("")
        while (currencyString.length < MINIMUM_DECIMAL_PLACES) {
            currencyString = "0".plus(currencyString)
        }

        val decimalPointIndex = currencyString.length - 2
        currencyString = currencyString.substring(0, decimalPointIndex) + "." + currencyString.substring(decimalPointIndex)

        return currencyString
    }
}