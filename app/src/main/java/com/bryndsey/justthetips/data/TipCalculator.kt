package com.bryndsey.justthetips.data

import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TipCalculator @Inject constructor(
        pretipTotal: PretipTotal,
        tipPercentageUpdateListener: TipPercentageUpdateListener) {

    private val BASE_TIP_PERCENTAGE: Double = 0.15

    private val tipAmountsObservable: Observable<Tip>

    init {
        tipAmountsObservable = Observable.combineLatest(
                pretipTotal.getTotalStringUpdate(),
                tipPercentageUpdateListener.getTipPercentageUpdates(),
                BiFunction { total, tip -> calculateTips(total, tip) }
        )
    }

    private fun calculateTips(inputSequence: String, tipPercentage: Double): Tip {
        val totalValue = inputSequence.toDouble()
        return Tip(
                BigDecimal(totalValue, MathContext(10, RoundingMode.HALF_UP))
                        .setScale(2, BigDecimal.ROUND_HALF_UP),
                tipPercentage)
    }

    fun getTipAmountUpdates(): Observable<Tip> {
        return tipAmountsObservable.observeOn(Schedulers.computation())
    }
}