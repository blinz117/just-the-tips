package com.bryndsey.justthetips.data

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PretipTotal @Inject constructor(
        private val inputToCurrencyTransformer: InputToCurrencyTransformer) {

    private val inputStack = mutableListOf<Int>()
    private val pretipTotalRelay = BehaviorRelay.create<List<Int>>()

    init {
        broadcastLatestUpdate()
    }

    fun inputValue(value: Int) {
        if (!isLeadingZero(value)) {
            inputStack.add(value)
            broadcastLatestUpdate()
        }
    }

    private fun isLeadingZero(input: Int): Boolean {
        return input == 0 && inputStack.size == 0
    }

    fun deleteLastInput() {
        if (inputStack.size > 0) {
            inputStack.removeAt(inputStack.lastIndex)
            broadcastLatestUpdate()
        }
    }

    private fun broadcastLatestUpdate() {
        pretipTotalRelay.accept(inputStack.toList())
    }

    fun getTotalUpdate(): Observable<List<Int>> {
        return pretipTotalRelay
    }

    fun getTotalStringUpdate(): Observable<String> {
        return pretipTotalRelay
                .map { inputList ->
                    inputToCurrencyTransformer.getCurrencyStringFromInput(inputList)
                }
    }
}