package com.bryndsey.justthetips.data

import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode


class Tip(val pretipTotal: BigDecimal, val tipPercentage: Double) {

    val tipAmount: BigDecimal
        get() = BigDecimal(
                    pretipTotal.toDouble() * tipPercentage,
                    MathContext(10, RoundingMode.HALF_UP))
                .setScale(2, BigDecimal.ROUND_HALF_UP)

    val total: BigDecimal
        get() = pretipTotal + tipAmount
}