package com.bryndsey.justthetips.data

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TipPercentageUpdateListener @Inject constructor() {

    private val relay = BehaviorRelay.create<Double>()

    fun updateTipPercentage(tipPercentage: Double) {
        relay.accept(tipPercentage)
    }

    fun getTipPercentageUpdates(): Observable<Double> {
        return relay
    }
}