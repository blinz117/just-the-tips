package com.bryndsey.justthetips.input.tipinput

import com.bryndsey.justthetips.data.TipPercentageUpdateListener
import com.bryndsey.justthetips.input.InputSwitcherListener
import com.bryndsey.justthetips.input.InputType
import easymvp.AbstractPresenter
import javax.inject.Inject

class TipInputPresenter @Inject constructor(
        private val inputSwitcherListener: InputSwitcherListener,
        private val tipPercentageUpdateListener: TipPercentageUpdateListener)
    : AbstractPresenter<TipInputViewInterface>() {

    val DIVISIONS_PER_PERCENT = 10
    val MIN_PERCENTAGE = 5
    val MAX_PERCENTAGE = 25
    val DEFAULT_PERCENTAGE = 15

    var currentPercentValue = (DEFAULT_PERCENTAGE - MIN_PERCENTAGE) * DIVISIONS_PER_PERCENT

    override fun onViewAttached(view: TipInputViewInterface?) {
        super.onViewAttached(view)

        view?.setPercentRange((MAX_PERCENTAGE - MIN_PERCENTAGE) * DIVISIONS_PER_PERCENT)
        view?.setCurrentPercentProgress(currentPercentValue)
        updateDisplay()
        updatePercentage()
    }

    fun switchToTotalInput() {
        inputSwitcherListener.switchInputType(InputType.TOTAL)
    }

    fun updateTipProgress(progress: Int) {
        currentPercentValue = progress
        updateDisplay()

        updatePercentage()
    }

    private fun updatePercentage() {
        tipPercentageUpdateListener.updateTipPercentage(
                getPercentValueForProgressValue(currentPercentValue) / 100.0)
    }

    private fun updateDisplay() {
        view?.updateTipPercentageDisplay(getPercentValueForProgressValue(currentPercentValue).toString() + "%")
    }

    fun getPercentValueForProgressValue(progress: Int): Double {
        return (progress.toDouble() / DIVISIONS_PER_PERCENT.toDouble()) + MIN_PERCENTAGE.toDouble()
    }
}