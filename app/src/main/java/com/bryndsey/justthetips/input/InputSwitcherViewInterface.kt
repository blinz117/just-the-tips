package com.bryndsey.justthetips.input


interface InputSwitcherViewInterface {

    fun showTotalInput()

    fun showTipInput()
}