package com.bryndsey.justthetips.input


enum class InputType {
    TOTAL,
    TIP
}