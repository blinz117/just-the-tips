package com.bryndsey.justthetips.input

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InputSwitcherListener @Inject constructor() {

    private val relay = BehaviorRelay.create<InputType>()

    fun switchInputType(inputType: InputType) {
        relay.accept(inputType)
    }

    fun getInputTypeUpdates(): Observable<InputType> {
        return relay
    }
}