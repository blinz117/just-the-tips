package com.bryndsey.justthetips.input.tipinput


interface TipInputViewInterface {

    fun setPercentRange(rangeSize: Int)

    fun updateTipPercentageDisplay(tipDisplayString: String)

    fun setCurrentPercentProgress(progress: Int)
}