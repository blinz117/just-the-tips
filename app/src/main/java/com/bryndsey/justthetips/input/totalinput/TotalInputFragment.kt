package com.bryndsey.justthetips.input.totalinput

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bryndsey.justthetips.JustTheTipsApplication
import com.bryndsey.justthetips.R
import easymvp.annotation.FragmentView
import easymvp.annotation.Presenter
import kotlinx.android.synthetic.main.total_input.*
import javax.inject.Inject

@FragmentView(presenter = TotalInputPresenter::class)
class TotalInputFragment : Fragment(), TotalInputView {

    @Inject
    @Presenter
    lateinit var presenter: TotalInputPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        JustTheTipsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.total_input, container)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        key_1.setOnClickListener { presenter.inputValue(1) }
        key_2.setOnClickListener { presenter.inputValue(2) }
        key_3.setOnClickListener { presenter.inputValue(3) }
        key_4.setOnClickListener { presenter.inputValue(4) }
        key_5.setOnClickListener { presenter.inputValue(5) }
        key_6.setOnClickListener { presenter.inputValue(6) }
        key_7.setOnClickListener { presenter.inputValue(7) }
        key_8.setOnClickListener { presenter.inputValue(8) }
        key_9.setOnClickListener { presenter.inputValue(9) }
        key_0.setOnClickListener { presenter.inputValue(0) }
        key_delete.setOnClickListener { presenter.deleteLastInput() }
        key_tip_percent.setOnClickListener { presenter.switchToTipInput() }
    }
}