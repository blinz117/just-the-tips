package com.bryndsey.justthetips.input.tipinput

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.bryndsey.justthetips.JustTheTipsApplication
import com.bryndsey.justthetips.R
import easymvp.annotation.FragmentView
import easymvp.annotation.Presenter
import kotlinx.android.synthetic.main.tip_input.*
import javax.inject.Inject

@FragmentView(presenter = TipInputPresenter::class)
class TipInputView : Fragment(), TipInputViewInterface {

    @Inject
    @Presenter
    lateinit var presenter: TipInputPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        JustTheTipsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.tip_input, container)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        total_input_switcher.setOnClickListener { presenter.switchToTotalInput() }

        tip_slider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    presenter.updateTipProgress(progress)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
    }

    override fun setPercentRange(rangeSize: Int) {
        tip_slider.max = rangeSize
    }

    override fun updateTipPercentageDisplay(tipDisplayString: String) {
        tip_percent_display.text = tipDisplayString
    }

    override fun setCurrentPercentProgress(progress: Int) {
        tip_slider.progress = progress
    }
}