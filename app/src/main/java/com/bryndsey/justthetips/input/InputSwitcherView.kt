package com.bryndsey.justthetips.input

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bryndsey.justthetips.JustTheTipsApplication
import com.bryndsey.justthetips.R
import com.bryndsey.justthetips.input.tipinput.TipInputView
import com.bryndsey.justthetips.input.totalinput.TotalInputFragment
import easymvp.annotation.FragmentView
import easymvp.annotation.Presenter
import javax.inject.Inject

@FragmentView(presenter = InputSwitcherPresenter::class)
class InputSwitcherView : Fragment(), InputSwitcherViewInterface {

    @Inject
    @Presenter
    lateinit var presenter: InputSwitcherPresenter

    private lateinit var totalInputView: TotalInputFragment
    private lateinit var tipInputView: TipInputView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        JustTheTipsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.input, container)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO: Figure out if there is a better way to get these
        // At minimum, I should use a string resource or something for the tags
        // But I'd rather access these via other means
        totalInputView = childFragmentManager.findFragmentByTag("total_input") as TotalInputFragment
        tipInputView = childFragmentManager.findFragmentByTag("tip_input") as TipInputView
    }

    override fun showTotalInput() {
        childFragmentManager.beginTransaction()
                .show(totalInputView)
                .hide(tipInputView)
                .commit()
    }

    override fun showTipInput() {
        childFragmentManager.beginTransaction()
                .show(tipInputView)
                .hide(totalInputView)
                .commit()
    }
}