package com.bryndsey.justthetips.input.totalinput

import com.bryndsey.justthetips.data.PretipTotal
import com.bryndsey.justthetips.input.InputSwitcherListener
import com.bryndsey.justthetips.input.InputType
import easymvp.AbstractPresenter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TotalInputPresenter @Inject constructor(
        private val pretipTotal: PretipTotal,
        private val inputSwitcherListener: InputSwitcherListener)
    : AbstractPresenter<TotalInputView>() {

    fun inputValue(value: Int) {
        pretipTotal.inputValue(value)
    }

    fun deleteLastInput() {
        pretipTotal.deleteLastInput()
    }

    fun switchToTipInput() {
        inputSwitcherListener.switchInputType(InputType.TIP)
    }
}