package com.bryndsey.justthetips.input

import easymvp.RxPresenter
import javax.inject.Inject

class InputSwitcherPresenter @Inject constructor(inputSwitcherListener: InputSwitcherListener)
    : RxPresenter<InputSwitcherViewInterface>() {

    private var currentInputType = InputType.TOTAL

    init {
        val subscription = inputSwitcherListener.getInputTypeUpdates()
                .subscribe { newInputType -> updateInputType(newInputType) }

        addSubscription(subscription)
    }

    private fun updateInputType(inputType: InputType) {
        currentInputType = inputType
        updateView()
    }

    private fun updateView() {
        when(currentInputType) {
            InputType.TOTAL -> view?.showTotalInput()
            InputType.TIP -> view?.showTipInput()
        }
    }

    override fun onViewAttached(view: InputSwitcherViewInterface?) {
        super.onViewAttached(view)

        updateView()
    }
}