package com.bryndsey.justthetips

import android.app.Application
import com.bryndsey.justthetips.di.ApplicationComponent
import com.bryndsey.justthetips.di.DaggerApplicationComponent

class JustTheTipsApplication : Application() {
    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder().build()
    }
}