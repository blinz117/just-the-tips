package com.bryndsey.justthetips.input.totalinput

import com.bryndsey.justthetips.data.PretipTotal
import com.bryndsey.justthetips.input.InputSwitcherListener
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test

class TotalInputPresenterTest {

    private lateinit var presenter: TotalInputPresenter
    private lateinit var mockPretipTotal: PretipTotal
    private lateinit var mockInputSwitchListener: InputSwitcherListener

    @Before
    fun setUp() {
        mockInputSwitchListener = mock<InputSwitcherListener>()
        mockPretipTotal = mock<PretipTotal>()
        presenter = TotalInputPresenter(mockPretipTotal, mockInputSwitchListener)
    }

    @Test
    fun inputValue_callsPretipTotalInputValue() {
        val inputValue = 0
        presenter.inputValue(inputValue)

        verify(mockPretipTotal).inputValue(inputValue)
    }
    
    @Test
    fun deleteLastInput_callsPretipTotalDeleteLastInput() {
        presenter.deleteLastInput()

        verify(mockPretipTotal).deleteLastInput()
    }
}