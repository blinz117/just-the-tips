package com.bryndsey.justthetips.data

import com.nhaarman.mockito_kotlin.mock
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test

class PretipTotalTest {

    private lateinit var pretipTotal: PretipTotal
    private lateinit var testObserver: TestObserver<List<Int>>
    private lateinit var mockInputToCurrencyTransformer: InputToCurrencyTransformer

    private val emptyList = mutableListOf<Int>()

    @Before
    fun setUp() {
        mockInputToCurrencyTransformer = mock<InputToCurrencyTransformer>()
        pretipTotal = PretipTotal(mockInputToCurrencyTransformer)
        testObserver = pretipTotal.getTotalUpdate().test()
    }

    @Test
    fun emitsEmptyInputListOnCreation() {
        testObserver.assertValue(emptyList)
    }

    @Test
    fun inputValue_addsValueAndUpdates() {
        val valueToInput = 1
        pretipTotal.inputValue(valueToInput)

        testObserver.assertValues(emptyList, mutableListOf(valueToInput))
    }

    @Test
    fun inputLeadingZero_doesNotUpdate() {
        pretipTotal.inputValue(0)

        testObserver.assertValue(emptyList)
    }

    @Test
    fun inputNonLeadingZero_updates() {
        val nonzeroValueToInput = 7
        pretipTotal.inputValue(nonzeroValueToInput)
        pretipTotal.inputValue(0)

        testObserver.assertValues(
                emptyList,
                mutableListOf(nonzeroValueToInput),
                mutableListOf(nonzeroValueToInput, 0)
        )
    }

    @Test
    fun deleteValueWithNoInputs_doesNotUpdate() {
        pretipTotal.deleteLastInput()

        testObserver.assertValue(emptyList)
    }

    @Test
    fun deleteValueAfterInput_updates() {
        val valueToInput = 1
        pretipTotal.inputValue(valueToInput)
        pretipTotal.deleteLastInput()

        testObserver.assertValueCount(3)
                .assertValues(
                        emptyList,
                        mutableListOf(valueToInput),
                        emptyList)
    }
}